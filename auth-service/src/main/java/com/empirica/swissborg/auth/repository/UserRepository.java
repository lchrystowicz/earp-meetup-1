package com.empirica.swissborg.auth.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.empirica.swissborg.auth.domain.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

}
