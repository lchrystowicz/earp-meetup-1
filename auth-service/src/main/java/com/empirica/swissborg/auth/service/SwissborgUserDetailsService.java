package com.empirica.swissborg.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.empirica.swissborg.auth.domain.User;
import com.empirica.swissborg.auth.repository.UserRepository;

@Service
public class SwissborgUserDetailsService implements UserDetailsService {

	private final UserRepository userRepository;

	@Autowired
	public SwissborgUserDetailsService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = userRepository.findOne(username);

		if (user == null) {
			throw new UsernameNotFoundException(username);
		}

		return user;
	}

}
