package com.empirica.swissborg.auth.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.empirica.swissborg.auth.domain.User;
import com.empirica.swissborg.auth.domain.exception.UserAlreadyExistsException;
import com.empirica.swissborg.auth.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/current", method = RequestMethod.GET)
	public Principal getUser(Principal principal) {
		return principal;
	}

	// @PreAuthorize("#oauth2.hasScope('backend')")
	@RequestMapping(method = RequestMethod.POST)
	public void createUser(@Valid @RequestBody User user) {
		try {
			userService.create(user);
		} catch (UserAlreadyExistsException e) {
			e.printStackTrace();
		}
	}

}
