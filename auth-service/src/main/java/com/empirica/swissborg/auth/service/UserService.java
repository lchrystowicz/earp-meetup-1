package com.empirica.swissborg.auth.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.empirica.swissborg.auth.domain.User;
import com.empirica.swissborg.auth.domain.exception.UserAlreadyExistsException;
import com.empirica.swissborg.auth.repository.UserRepository;

@Service
public class UserService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final UserRepository userRepository;

	private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	@Autowired
	public UserService(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	public void create(User user) throws UserAlreadyExistsException {
		User foundUser = userRepository.findOne(user.getUsername());

		if (userExists(foundUser)) {
			throw new UserAlreadyExistsException(user.getUsername());
		}

		String hash = encoder.encode(user.getPassword());
		user.setPassword(hash);

		userRepository.save(user);

		log.info("new user has been created: {}", user.getUsername());

	}

	private boolean userExists(User foundUser) {
		return foundUser != null;
	}

}
